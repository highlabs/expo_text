import React from 'react';

import {
    View,
    StyleSheet
} from 'react-native';

import {
  Content,
  Text
} from "native-base";

// Icons
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/project_icons.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'Icons');

import { DrawerHeader } from '../components/DrawerHeader'

export default class Empty extends React.Component {
  static navigationOptions = {
    title: 'EmptyTabScren',
    headerStyle: {
        backgroundColor: '#3232a8'
    },
    headerTintColor: '#fff',
  };

  constructor() {
    super();
    this.state = {};
  };

  render() {

    return (
      <View style={styles.container}>
        <DrawerHeader navigation={this.props.navigation}/>
        <Content contentContainerStyle={{ flex: 1 }}>
          <Text>Tab Screen</Text>
        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
    }
  });