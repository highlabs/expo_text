import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {StyleSheet, View, Image} from 'react-native';
import { Col, Row, Grid, Text, Content, Button } from "native-base";

// Icons
import { EvilIcons, createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/project_icons.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'Icons');

import {NavigationActions} from 'react-navigation';
import { ScrollView } from 'react-native-gesture-handler';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
        <Content contentContainerStyle={styles.background}>
          <Grid style={{height: '100%', flex: 1}}>
            <Row style={styles.header}>
              <Col style={{justifyContent: 'space-between'}}>
                <Image
                  source={require('../assets/images/sem_parar_logo.png')}
                  fadeDuration={0}
                  style={{width: 46, height: 37}}
                />
                <Text style={styles.title}>Olá, Guilherme!</Text>
              </Col>
              <Col>
                <Button
                  transparent
                  style={{alignSelf: 'flex-end', paddingTop: 0, height: 30}}
                  onPress={() => {
                    this.props.navigation.navigate('DrawerToggle');
                  }}>
                  <EvilIcons name="close" color="#fd0000"  size={30}/>
                </Button>
              </Col>
            </Row>

            <Row style={[styles.menuContainer]}>
              <Col>
                <Text onPress={() => this.props.navigation.navigate('Home')} style={styles.menuItem}><Icon style={styles.iconMargin} color="#FFF" name='icon-person'/> Meus Dados</Text>
                <Text style={styles.menuItem}><Icon style={styles.iconMargin} color="#FFF" name='icon-invite'/> Meus Planos</Text>
                <Text style={styles.menuItem}><Icon style={styles.iconMargin} color="#FFF" name='icon-barcode'/> Ativar Sem Parar</Text>
                <Text style={styles.menuItem}><Icon style={styles.iconMargin} color="#FFF" name='icon-bell'/> Mensagens</Text>
                <Text style={styles.menuItem}><Icon style={styles.iconMargin} color="#FFF" name='icon-share'/> Convidar Amigos</Text>
              </Col>
              <Col style={{justifyContent: 'flex-end'}}>
                <Text style={styles.menuItemGray}><Icon style={styles.iconMargin} color="#b5b5b5" name='icon-help'/> Ajuda</Text>
                <Text style={styles.menuItemGray}><Icon style={styles.iconMargin} color="#b5b5b5" name='icon-gear'/> Configurações</Text>
                <View style={{
                  height: 1,
                  width: '100%',
                  borderTopColor: '#b5b5b5',
                  borderTopWidth: 1,
                  marginTop: 20
                }}></View>
                <Text style={{
                  color: '#ffffff',
                  fontFamily: 'Roboto Light',
                  fontSize: 15,
                  fontWeight: '300',
                  marginTop: 20
                }}><Icon style={styles.iconMargin} color="#FFF" name='icon-menu-logout'/> Sair</Text>
              </Col>
            </Row>
          </Grid>
        </Content>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

const styles = StyleSheet.create({
  title: {
    color: '#333333',
    fontFamily: 'Roboto',
    fontSize: 16,
    fontWeight: '400',
  },
  background:{
    backgroundColor: '#333333',
    minHeight: '100%',
  },
  menuContainer: {
    paddingTop: 28,
    paddingRight: 36,
    paddingBottom: 37,
    paddingLeft: 39,
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
    flex: 1
  },
  header: {
    paddingTop: 19,
    paddingRight: 19,
    paddingBottom: 36,
    paddingLeft: 36,
    backgroundColor: '#fff'
  },
  menuItem: {
    color: '#ffffff',
    fontFamily: 'Roboto Light',
    fontSize: 18,
    fontWeight: '300',
    marginBottom: 23,
  },
  menuItemGray: {
    color: '#b5b5b5',
    fontFamily: 'Roboto Light',
    fontSize: 15,
    fontWeight: '300',
    marginTop: 20
  },
  iconMargin: {
    marginRight: 9
  }
});

export default SideMenu;
