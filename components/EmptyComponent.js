import React from 'react';

import {
    View,
    StyleSheet
} from 'react-native';

import {
  Content
} from "native-base";

// Icons
import { MaterialCommunityIcons, createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/project_icons.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'Icons');

import { ScrollView } from 'react-native-gesture-handler';

export class EmptyComponentName extends React.Component {

  constructor() {
    super();
    this.state = {};
  };

  render() {

    return (
      <View style={styles.container}>
        <Content contentContainerStyle={{ flex: 1 }}>

        </Content>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
    }
  });