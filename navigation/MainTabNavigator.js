import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Entypo,createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/project_icons.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'Icons');
import { TabNavigator, TabBarBottom, DrawerNavigator } from 'react-navigation';
import Colors from '../constants/Colors';


import SideMenu from '../components/SideMenu';

import Screen1 from '../screens/TabEmptyScreen';
import Screen2 from '../screens/TabEmptyScreen';
import Screen3 from '../screens/TabEmptyScreen';
import Screen4 from '../screens/TabEmptyScreen';

export const Tabs =  TabNavigator(
  {

    Screen1: {
      screen: Screen1,
      
    },
    Screen2: {
      screen: Screen2,
      
    },
    Screen3: {
      screen: Screen3,
      
    },
    Screen4: {
      screen: Screen4,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Screen1':
            iconName = 'icon-flag';
            break;
          case 'Screen2':
            iconName = 'icon-my-use';
            break;
          case 'Screen3':
            iconName = 'icon-services'
            break;
          case 'Screen4':
            iconName = 'icon-car2'
        }
        return (
          <Icon
            name={iconName}
            size={20}
            style={{ marginBottom: -3, width: 25 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },

      headerLeft: 
      <TouchableOpacity
        onPress={() => {
          if(navigation.state.index === 0) {
            navigation.navigate('DrawerOpen');
          } else {
            navigation.navigate('DrawerClose');
          }
        }}
      >
        <View style={{ padding: 10 }}>
          <Entypo name={navigation.state.index === 0 ? 'menu' : 'chevron-small-left'} size={32} color="#000" />
        </View>
      </TouchableOpacity>
    }),
    tabBarVisible: true,
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

export default DrawerNavigator({
  Tabs: {
    screen: Tabs,
  }
}, {
  contentComponent: SideMenu,
  backBehavior: 'none'
})