# How to Start Project

Steps:

1. Install yarn (https://yarnpkg.com/lang/en/docs/install/)[https://yarnpkg.com/lang/en/docs/install/]
2. Install Expo XDE (https://docs.expo.io/versions/latest/introduction/installation)[https://docs.expo.io/versions/latest/introduction/installation]
3. Run project from XDE

### Creating Screens

Using the templates files (.xd or .ai). Copy and paste `/Screes/EmptyScreen` or `/components/EmptyComponent` as boilerplate.

These files are commented. Please keep the default styles from these files.

### Third party packages

Use only the packages already installed. If you need to create to install a new package, please contact me.